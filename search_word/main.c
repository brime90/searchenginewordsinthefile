
#include "myData.h"

void* fileRead_beginToEnd(void *args){
    FILE *fd = NULL;
    struct Arguments *myArg = (struct Arguments *)args;
    char *buf;

    fd = fopen64(myArg->file_path, "r+");

	if (fd == NULL){
		printMessageToConsole("Cannot open file \n");
	}
    else{

        myFuncInfoSizeFile__init(fd);

        while (!feof(fd) && !getStatusResult() && !getStatusSearch()){
            buf = (char *)malloc(sizeof(char)*BUFFER_SIZE);
            fgets(buf, BUFFER_SIZE , fd);

            while(myQueueStr.count >= MAX_LIMIT_QUEUE && !getStatusResult() && !getStatusSearch()) usleep(DELAY_SLEEP_THREAD);

            add_toQueue(&mutexListFile, &myQueueStr, buf);

            printStatusReadFile(fd);
        }
        fclose (fd);
    }

    setStatusRead(true);

    return SUCCESS;
}

void* fileRead_endToBegin(void *args){
    FILE *fd = NULL;
    struct Arguments *myArg = (struct Arguments *)args;
    char *buf;
    off_t blockRead = BUFFER_SIZE, pos_cur, pos_end;
    off_t len = 0;

    fd = fopen64(myArg->file_path, "r+");

	if (fd == NULL){
		printMessageToConsole("Cannot open file \n");
	}
    else{
        myFuncInfoSizeFile__init(fd);

        if(myFuncInfoSizeFile__get() < 1){
            printMessageToConsole("Error in program....\n");
        }
        else{
            pos_cur = myFuncInfoSizeFile__get() > 0 ? myFuncInfoSizeFile__get() - 1 : 0;

            while(pos_cur > 0 && !getStatusResult() && !getStatusSearch()){
                blockRead = BUFFER_SIZE;
                pos_end = pos_cur + 1;
                pos_cur -= pos_cur > blockRead ? blockRead : pos_cur;
                lseek64(fd->_fileno, pos_cur, SEEK_SET);
                blockRead = pos_end - pos_cur;
                while (blockRead > 1 && !getStatusResult() && !getStatusSearch()){
                    buf = (char *)malloc(sizeof(char)*blockRead);
                    fgets(buf, blockRead , fd);
                    len = strlen(buf);
                    blockRead -= len;

                    while(myQueueStr.count >= MAX_LIMIT_QUEUE && !getStatusResult() && !getStatusSearch()) usleep(DELAY_SLEEP_THREAD);

                    add_toQueue(&mutexListFile, &myQueueStr, buf);

                    printStatusReadFile(fd);
                }
            }
            fclose (fd);
        }
    }

    setStatusRead(true);

    return SUCCESS;
}

void* selectTypeReadFile(void *args){
    struct Arguments *myArg = (struct Arguments *)args;

    if(myArg->scan_tail == true) fileRead_endToBegin(args);
    else                         fileRead_beginToEnd(args);

    return SUCCESS;
}

void* findWordByMask(void *args){
    struct Arguments *myArg = (struct Arguments *)args;
    char *strFromFile = NULL;
    char *subStr = NULL;
    size_t offset = 0;
    regmatch_t p;
    regex_t f;

    if(regcomp(&f, myArg->mask, REG_EXTENDED|REG_ICASE)){
        printMessageToConsole("error in the template reg! \n");
    }
    else{
        while(!getStatusResult()){

            while(myQueueStr.count < 1 && !getStatusRead()){  usleep(DELAY_SLEEP_THREAD); }

            strFromFile = get_fromQueue(&mutexListFile, &myQueueStr);

            if(strFromFile == NULL) break;

            offset = 0;
            while(regexec(&f, strFromFile + offset, 1, &p, 0) == 0){
                while(myQueueResult.count >= MAX_LIMIT_QUEUE && !getStatusResult()) usleep(DELAY_SLEEP_THREAD);

                subStr = getSubString(strFromFile, p.rm_so + offset, p.rm_eo -  p.rm_so);

                if(subStr == NULL) break;

                add_toQueue(&mutexListResult, &myQueueResult, subStr);
                offset += p.rm_eo;
            }

            free(strFromFile);
        }
    }

    setStatusSearch(true);

    return SUCCESS;
}


void* writeResult(void *args){
    struct Arguments *myArg = (struct Arguments *)args;
    char *resultStr = NULL, *ptrString = NULL, *strFromQueue = NULL;
    size_t sizeSrc = 0;
    size_t sizeAdd = 0;
    size_t sizeSep = strlen(myArg->separator);
    size_t totalStr = 0;

    while(totalStr < myArg->max_lines){

        while(myQueueResult.count < 1 && !getStatusSearch()){  usleep(DELAY_SLEEP_THREAD); }

        strFromQueue = get_fromQueue(&mutexListResult, &myQueueResult);

        if(strFromQueue == NULL) break;

        sizeAdd = strlen(strFromQueue);

        ptrString = addString(resultStr, strFromQueue, myArg->separator, sizeSrc, sizeAdd, sizeSrc > 0 ? sizeSep : 0);

        free(strFromQueue);

        if(ptrString != NULL){
            sizeSrc += sizeSrc > 0 ? (sizeAdd + sizeSep) : sizeAdd;
            resultStr = ptrString;
            totalStr++;
        }
        else{
            break;
        }

    }

    if(resultStr != NULL)resultStr[sizeSrc] = '\0';

    setStatusResult(true);
    return (void*)resultStr;
}


char* log_search(const char* file_path, const char* mask, size_t max_lines, size_t scan_tail, const char* separator){
    int status;
    pthread_t thread_readFile;
    pthread_t thread_search;
    pthread_t thread_writeRezult;
    char *rez = NULL;

    struct Arguments myArg = {file_path, mask, max_lines, scan_tail, separator};

    status = pthread_create(&thread_readFile, NULL, selectTypeReadFile, &myArg);
    if (status != 0) {
        printf("can't create thread_readFile, status = %d\n", status);
        return NULL;
    }

    status = pthread_create(&thread_search, NULL, findWordByMask, &myArg);
    if (status != 0) {
        printf("can't create thread_search, status = %d\n", status);
        pthread_exit(&thread_readFile);
        return NULL;
    }

    status = pthread_create(&thread_writeRezult, NULL, writeResult, &myArg);
    if (status != 0) {
        printf("can't create thread_writeRezult, status = %d\n", status);
        pthread_exit(&thread_readFile);
        pthread_exit(&thread_search);
        return NULL;
    }

    pthread_join(thread_readFile, NULL);
    pthread_join(thread_search, NULL);
    pthread_join(thread_writeRezult, (void **)&rez);

    return rez;
}

int main(int argc, char * argv[]) {

    char *resultStr = NULL;

    if(argc < 4){
        printf("Too few arguments\n");
        printf("use: file_path, mask, max_lines, scan_tail, separator\n");
        return 1;
    }

    size_t max_line = atoi(argv[3]);
    size_t scan_tail = false;

    if(argc >= 5){
        if(!strcmp(argv[4], "TRUE") || !strcmp(argv[4], "true") || !strcmp(argv[4], "1")) scan_tail = true;
    }

    resultStr = log_search(argv[1], argv[2], max_line < MAX_LINE ? max_line : MAX_LINE, scan_tail,  argc >= 6 ? argv[5] : "");

    size_t len = strlen(resultStr);

    if(resultStr != NULL){
        printf("\n\n %s", resultStr);
    }


    clear_Queue(&myQueueStr);
    clear_Queue(&myQueueResult);
    free(resultStr);
    return 0;
}
