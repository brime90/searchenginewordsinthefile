
#include "myData.h"


struct Queue myQueueStr = {0, NULL, NULL};
struct Queue myQueueResult = {0, NULL, NULL};

pthread_mutex_t mutexListFile = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexListResult = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t mutexStatusRead = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexStatusSearch = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexStatusResult = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t mutexShowMessage = PTHREAD_MUTEX_INITIALIZER;

bool PTHREAD_READ_FINISH = false;
bool PTHREAD_SEARCH_FINISH = false;
bool PTHREAD_RESULT_FINISH = false;

static off_t sizeFile = 0;

void myFuncInfoSizeFile__init(FILE *fd){
    off_t pos = 0;

    pos = lseek64(fd->_fileno, (off_t)0, SEEK_CUR);
    if(pos >= 0) sizeFile = lseek64(fd->_fileno, (off_t)0, SEEK_END);
    if(pos >= 0 && sizeFile >= 0)pos = lseek64(fd->_fileno, pos, SEEK_SET);
}

off_t myFuncInfoSizeFile__get(void){
    return sizeFile;
}

void printMessageToConsole(char *msg){

    pthread_mutex_lock(&mutexShowMessage);
    printf("\n %s", msg);
    pthread_mutex_unlock(&mutexShowMessage);
}

void printStatusReadFile(FILE *fd)
{
    static size_t clock = 0;

    if(clock++ > 100000){
        off_t size = myFuncInfoSizeFile__get();
        off_t pos = lseek64(fd->_fileno, (off_t)0, SEEK_CUR);
        off_t perc = (((size - pos)*100)/size);

        pthread_mutex_lock(&mutexShowMessage);
        printf("\n Status %3llu%%.....", perc);
        printf("\033[1F");
        pthread_mutex_unlock(&mutexShowMessage);
        clock = 0;
    }
}

bool getStatusRead(){
    bool ret;

    pthread_mutex_lock(&mutexStatusRead);
    ret = PTHREAD_READ_FINISH;
    pthread_mutex_unlock(&mutexStatusRead);

    return ret;
}

bool getStatusSearch(){
    bool ret;

    pthread_mutex_lock(&mutexStatusSearch);
    ret = PTHREAD_SEARCH_FINISH;
    pthread_mutex_unlock(&mutexStatusSearch);

    return ret;
}

bool getStatusResult(){
    bool ret;

    pthread_mutex_lock(&mutexStatusResult);
    ret = PTHREAD_RESULT_FINISH;
    pthread_mutex_unlock(&mutexStatusResult);

    return ret;
}

void setStatusRead(bool val){

    pthread_mutex_lock(&mutexStatusRead);
    PTHREAD_READ_FINISH = val;
    pthread_mutex_unlock(&mutexStatusRead);
}

void setStatusSearch(bool val){

    pthread_mutex_lock(&mutexStatusSearch);
    PTHREAD_SEARCH_FINISH = val;
    pthread_mutex_unlock(&mutexStatusSearch);
}

void setStatusResult(bool val){

    pthread_mutex_lock(&mutexStatusResult);
    PTHREAD_RESULT_FINISH = val;
    pthread_mutex_unlock(&mutexStatusResult);
}

void add_toQueue(pthread_mutex_t *mutex, struct Queue *data, char *str){
    struct Data *temp = (struct Data *)calloc(1, sizeof(struct Data));

    pthread_mutex_lock(mutex);

    temp->str = str;
    temp->next = NULL;
    data->count++;

    if(data->begin == NULL && data->end == NULL){
        data->begin = temp; data->end = temp;
    }
    else{
        data->end->next = temp;
        data->end = temp;
    }

    pthread_mutex_unlock(mutex);
}

char* get_fromQueue(pthread_mutex_t *mutex, struct Queue *data){

    struct Data *temp = NULL;
    char *ret = NULL;

    pthread_mutex_lock(mutex);

    if(data->begin == NULL && data->end == NULL){
        temp = NULL;
    }
    else{
        temp = data->begin;

        if(data->begin == data->end){
            data->begin = data->end = NULL;
            data->count = 0;
        }
        else{
            data->begin = data->begin->next;
            data->count--;
        }
        ret = temp->str;
        free(temp);
    }


    pthread_mutex_unlock(mutex);

    return ret;
}


void clear_Queue(struct Queue *data){

    while(data->begin != NULL){
        struct Data *temp = data->begin;

        free(temp->str);
        if(data->begin == data->end){
            data->begin = data->end = NULL;
            data->count = 0;
        }
        else{
            data->begin = data->begin->next;
        }
        free(temp);
    }
}


char* addString(char *src, char *add, const char *separ, size_t size_src, size_t size_add, size_t size_separ){
    char *temp;
    temp = (char *)realloc(src, size_src + size_add + size_separ + (size_src > 0 ? 0 : sizeof(char)));

    if(temp != NULL){
        memcpy(temp, src, size_src);
        memcpy(temp+size_src, separ, size_separ);
        memcpy(temp+size_src+size_separ, add, size_add);
    }else{
        printf("\n Error allocation memory\n");
    }

    return temp;
}

char* getSubString(char *src, size_t ind, size_t count){
    size_t len = strlen(src);
    char *temp = NULL;
    int i = 0;

    if(ind + count < len){
        temp = (char *)calloc(count + 1, sizeof(char));
        strncpy(temp, src + ind, count);
        temp[count] = 0;
    }

    return temp;
}
