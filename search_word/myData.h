#ifndef MYDATA_H_INCLUDED
#define MYDATA_H_INCLUDED

#define _LARGEFILE64_SOURCE
#define _FILE_OFFSET_BITS 64

#include <pthread.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <regex.h>
#include <unistd.h>

#define SUCCESS        0

#define MAX_LIMIT_QUEUE     30000
#define BUFFER_SIZE         2000   //buffer readers file
#define MAX_LINE            1000
#define DELAY_SLEEP_THREAD  100000 //microseconds


struct Arguments{
    char *file_path;
    const char* mask;
    size_t max_lines;
    size_t scan_tail;
    const char* separator;
};

struct Data{
    struct Data *next;
    char* str;
};

struct  Queue{
    size_t count;
    struct Data *begin;
    struct Data *end;
};


extern struct Queue myQueueStr;
extern struct Queue myQueueResult;

extern pthread_mutex_t mutexListFile;
extern pthread_mutex_t mutexListResult;

extern pthread_mutex_t mutexStatusRead;
extern pthread_mutex_t mutexStatusSearch;
extern pthread_mutex_t mutexStatusResult;

extern bool PTHREAD_READ_FINISH;
extern bool PTHREAD_SEARCH_FINISH;
extern bool PTHREAD_RESULT_FINISH;

char*        addString(char *src, char *add, const char *separ, size_t size_src, size_t size_add, size_t size_separ);
void         clear_Queue(struct Queue *data);
char*        get_fromQueue(pthread_mutex_t *mutex, struct Queue *data);
void         add_toQueue(pthread_mutex_t *mutex, struct Queue *data, char *param);
void         printMessageToConsole(char *msg);
void         printStatusReadFile(FILE *fd);
off_t        myFuncInfoSizeFile__get(void);
void         myFuncInfoSizeFile__init(FILE *fd);
char*        getSubString(char *src, size_t ind, size_t count);

bool getStatusRead();
bool getStatusSearch();
bool getStatusResult();
void setStatusRead(bool val);
void setStatusSearch(bool val);
void setStatusResult(bool val);

#endif // MYDATA_H_INCLUDED
